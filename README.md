README

This project was made at Penn Apps Spring 2015 under collaboration of Avery Lamp, Sebastian Cain, Charley Hutchinson, and Sam Moore.  The app tracks a magnet after being calibrated and uses the magnet as a drawing tool.  It was created within 36 hours.  

http://challengepost.com/software/magic-pen

