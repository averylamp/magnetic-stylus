//
//  ToolbarView.m
//  MagnetStylus
//
//  Created by Sam Moore on 1/17/15.
//  Copyright (c) 2015 PennAppsTeam2015. All rights reserved.
//

#import "CalibrationView.h"

#define kBeginCalibrationText @"Calibrate"
#define kPositionCalibrationText @"Set Reference Point For Corner %d"

@interface CalibrationView () {
    BOOL singleButton;
}

@property (strong, nonatomic) UIView *toolbarView;
@property (strong, nonatomic) UIButton *calibrateButtonView;
@property (strong, nonatomic) UIButton *touchDrawingButtonView;
@property (strong, nonatomic) UIView *positionIdentifierView;

@property (readonly, nonatomic) NSUInteger quadrant;

@end

@implementation CalibrationView

#pragma mark - Boilerplate

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self == nil) return nil;
    
    // additional setup after initialization
    [self addSubview:self.toolbarView];
    [self addSubview:self.calibrateButtonView];
    [self addSubview:self.touchDrawingButtonView];
    
    return self;
}

- (void)layoutSubviews {
    CGRect frame = self.frame;
    
    int toolbarHeight = 80;
    CGSize buttonSize = CGSizeMake(150, 50);
    
    self.toolbarView.frame = (CGRect) {
        .origin = {
            0,
            frame.size.height - toolbarHeight
        },
        .size = {
            frame.size.width,
            toolbarHeight
        }
    };
    
    // we only want to set the origin and the default size before we change the text
    if (!singleButton || !CGPointEqualToPoint(self.calibrateButtonView.center, self.toolbarView.center)) {
        self.calibrateButtonView.frame = (CGRect) {
            .origin = {
                CGRectGetMaxX(self.toolbarView.frame) - buttonSize.width - 20,
                CGRectGetMidY(self.toolbarView.frame) - buttonSize.height/2
            },
            .size = buttonSize
        };
    } else {
        // we want to preserve the frame.size, set by -[self updateButtonWithText:]
        self.calibrateButtonView.frame = (CGRect) {
            .origin = {
                CGRectGetMidX(self.toolbarView.frame) - self.calibrateButtonView.frame.size.width/2,
                CGRectGetMidY(self.toolbarView.frame) - self.calibrateButtonView.frame.size.height/2
            },
            .size = self.calibrateButtonView.frame.size
        };
    }
    
    self.touchDrawingButtonView.frame = (CGRect) {
        .origin = {
            20,
            CGRectGetMidY(self.toolbarView.frame) - buttonSize.height/2
        },
        .size = buttonSize
    };
}

#pragma mark - Target/Action

- (void)buttonTapped {
    singleButton = YES;
    
    if ([self.calibrateButtonView.titleLabel.text isEqualToString:kBeginCalibrationText])
    // begin calibrating
    {
        [self.calibrationDelegate calibrationDidBegin];
        _quadrant = 1;
        [self updateButtonWithText:[NSString stringWithFormat:kPositionCalibrationText, (int)_quadrant, nil]];
    }
    else if (_quadrant == 4)
    // end calibrating
    {
		[self.calibrateButtonView setEnabled:NO];
        [self.calibrationDelegate calibrateForQuadrant:self.quadrant];
        [self updateButtonWithText:@"Start Drawing!"];
        [self.calibrationDelegate calibrationDidEnd];
    }
    else
    // next step
    {
        [self.calibrationDelegate calibrateForQuadrant:self.quadrant];
        _quadrant++;
        [self updateButtonWithText:[NSString stringWithFormat:kPositionCalibrationText, (int)_quadrant, nil]];
    }
}

- (void)touchDrawingChosen {
    NSLog(@"touch bitch!");
    [self.calibrationDelegate calibrationUserOptedForTouchDrawing];
    [self.calibrationDelegate calibrationDidEnd];
}

#pragma mark - Subviews

- (UIView *)toolbarView {
    if (_toolbarView == nil) {
        _toolbarView = [[UIView alloc] initWithFrame:CGRectZero];
        _toolbarView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    }
    return _toolbarView;
}

- (UIButton *)calibrateButtonView {
    if (_calibrateButtonView == nil) {
        _calibrateButtonView = [[UIButton alloc] initWithFrame:CGRectZero];
        _calibrateButtonView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1];
        
        [_calibrateButtonView setTitle:@"Calibrate" forState:UIControlStateNormal];
        _calibrateButtonView.titleLabel.textColor = [UIColor blackColor];
        _calibrateButtonView.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        [_calibrateButtonView addTarget:self action:@selector(buttonTapped) forControlEvents:UIControlEventTouchUpInside];
    }
    return _calibrateButtonView;
}

- (UIButton *)touchDrawingButtonView {
    if (_touchDrawingButtonView == nil) {
        _touchDrawingButtonView = [[UIButton alloc] initWithFrame:CGRectZero];
        _touchDrawingButtonView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1];
        
        [_touchDrawingButtonView setTitle:@"Touch Drawing" forState:UIControlStateNormal];
        _touchDrawingButtonView.titleLabel.textColor = [UIColor blackColor];
        _touchDrawingButtonView.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        [_touchDrawingButtonView addTarget:self action:@selector(touchDrawingChosen) forControlEvents:UIControlEventTouchUpInside];
    }
    return _touchDrawingButtonView;
}

#pragma mark - Helpers

- (void)updateButtonWithText:(NSString *)text {
    if (text != nil) {
        [self.calibrateButtonView setTitle:text forState:UIControlStateNormal];
        CGRect frame = self.calibrateButtonView.frame;
        
        // attribute the string with the font we are using so we can calculate width
        NSAttributedString *string = [[NSAttributedString alloc]
            initWithString:text
            attributes:@{
                NSFontAttributeName: self.calibrateButtonView.titleLabel.font
            }];
        
        frame.size.width = string.size.width + 50;
        self.calibrateButtonView.frame = frame;
        
        [self setNeedsLayout];
    }
}

@end
