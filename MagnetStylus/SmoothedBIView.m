#import "SmoothedBIView.h"
#import "Magnet.h"
#import "CalibrationView.h"

@interface SmoothedBIView () <MagnetDelegate, CalibrationDelegate>
{
    
    CGPoint pts[5]; // we now need to keep track of the four points of a Bezier segment and the first control point of the next segment
    uint ctr;
    BOOL touchBasedDrawing;
    BOOL needsRedraw;
    BOOL startedDrawing;
}

@property (strong, nonatomic) Magnet *magnet;
@property (strong, nonatomic) CalibrationView *calibrationView;

@property (strong, nonatomic) UIBezierPath *path;
@property (strong, nonatomic) NSMutableArray *paths;
@property (strong, nonatomic) UIImage *incrementalImage;


@property (strong, nonatomic) UIView *cursorView;
@property (strong, nonatomic) UILabel *x;
@property (strong, nonatomic) UILabel *y;
@property (strong, nonatomic) UILabel *mag;
@property (strong, nonatomic) UILabel *angle;
@property (strong, nonatomic) UILabel *xVector;

@end

@implementation SmoothedBIView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
	if (self == nil) return nil;
    
    [self initialize];
    
    return self;
    
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self == nil)  return nil;
    
    [self initialize];
        
    return self;
}

- (void)initialize {
	[self setMultipleTouchEnabled:NO];
	[self.path setLineWidth:10.0];
	
    touchBasedDrawing = NO;
	needsRedraw = false;
	
    self.x = [[UILabel alloc] initWithFrame:CGRectZero];
	self.y = [[UILabel alloc] initWithFrame:CGRectZero];
    self.mag = [[UILabel alloc] initWithFrame:CGRectZero];
	self.angle = [[UILabel alloc] initWithFrame:CGRectZero];
    
	// add cursor
	[self addSubview:self.cursorView];
    [self addSubview:self.xVector];
    [self addSubview:self.x];
	[self addSubview:self.y];
	[self addSubview:self.mag];
    [self addSubview:self.angle];
    
    // set up weak properties here
	self.magnet = [Magnet sharedMagnet];
	self.magnet.delegate = self;
	
	// set up calibration view
	self.calibrationView.calibrationDelegate = self;
	[self addSubview:self.calibrationView];
}

- (void)layoutSubviews {
	self.calibrationView.frame = self.frame;
    
    self.xVector.frame = CGRectMake(0, 50, 200, 50);
    self.x.frame = CGRectMake(0, 100, 200, 50);
    self.y.frame = CGRectMake(0, 150, 200, 50);
    self.mag.frame = CGRectMake(0, 200, 200, 50);
    self.angle.frame = CGRectMake(0, 250, 200, 50);
}

#pragma mark - Delegates

#pragma mark -- Magnet Delegate

- (void)magnetDidUpdate {
//	NSLog(@"magnetDidUpdate ran");
    
	//NSLog(@"radius: %f", self.magnet.radius);
	// caused error?
	if (self.magnet.radius == NAN || (self.magnet.radius < 1 && self.magnet.radius > -1)) return;
	CGRect newFrame = self.cursorView.frame;
	
	CGSize screenSize = [UIScreen mainScreen].bounds.size;
	
	if (self.magnet.isCalibrated) {
		newFrame.origin = CGPointMake(
			screenSize.height *-1.3* ((self.magnet.angle - self.magnet.bottomLeftPoint.y)/(self.magnet.bottomLeftPoint.y- self.magnet.bottomRightPoint.y)),
			(screenSize.height*-2 * ((((self.magnet.radius - self.magnet.topLeftPoint.x) / (self.magnet.topLeftPoint.x-self.magnet.bottomLeftPoint.x)))))- screenSize.height);
	} else {
		newFrame.origin = CGPointMake(
			 screenSize.height * ((self.magnet.angle + 100)/50),
			 screenSize.height - (self.magnet.radius/2));
	}
	
	if (fabs(newFrame.origin.x) < 1 && fabs(newFrame.origin.y) < 1) {
		return;
	} else if (newFrame.origin.x == NAN || newFrame.origin.y == NAN) {
		return;
	}
    
	//
	//self.statsLabel.text = [NSString stringWithFormat:@"x: %f y: %f", newFrame.origin.x, newFrame.origin.y];
	
	self.cursorView.frame = newFrame;
	
	/*
	UIView *point = [[UIView alloc] initWithFrame:self.cursorView.frame];
	point.backgroundColor = UIColor.redColor;
	[self.view addSubview:point];
	 */
	CGPoint pointToAdd = newFrame.origin;
	//NSLog(@"point \n\t x: %f \n\t y: %f", pointToAdd.x, pointToAdd.y);
	
    self.xVector.text = [NSString stringWithFormat:@"zVector - %.1f", self.magnet.zVector ];
    
	self.x.text = [NSString stringWithFormat:@"Xcoord - %.1f",pointToAdd.x];
	
	self.y.text = [NSString stringWithFormat:@"Ycoord - %.1f",pointToAdd.y];
	
	self.mag.text = [NSString stringWithFormat:@"Madnitude - %.1ld",(long)self.magnet.calculatedMagnitude];
	
	self.angle.text = [NSString stringWithFormat:@"Angle - %.1f",self.magnet.angle];
	
	// if we aren't done calibrating, don't draw the bezier path
	if (self.magnet.isCalibrated == NO) {return;}
	
    if (CGRectContainsPoint(self.frame, pointToAdd) == false) {
        if (startedDrawing == YES) {
            [self endPointsWithPoint:pointToAdd];
            startedDrawing = NO;
        }
    }
    
	if (startedDrawing != YES && self.magnet.zVector > 50) {
		//NSLog(@"initial conditional true");
		return;
	} else if (startedDrawing != YES) {
		[self beginPointsWithPoint:pointToAdd];
		startedDrawing = YES;
	} else if (self.magnet.zVector > 50) {
		NSLog(@"endpoint method called");
		[self endPointsWithPoint:pointToAdd];
		startedDrawing = NO;
	} else {
		[self updatePointsWithPoint:pointToAdd];
	}
}

#pragma mark -- CalibrationViewDelegate

- (void)calibrationDidBegin {
	NSLog(@"calibration began");
	[self.magnet setBaselineFromCurrentState];
	[self updateCursorViewFrameForCalibrationWithQuadrant:1];
}

- (void)calibrateForQuadrant:(NSUInteger)quadrant {
	NSLog(@"calibration for quadrant %d", (int)quadrant);
	[self.magnet setCalibrationForQuadrant:quadrant];
	
	// quadrant is what we just calibrated so we start at 1 not 2
	[self updateCursorViewFrameForCalibrationWithQuadrant:++quadrant];
}

- (void)updateCursorViewFrameForCalibrationWithQuadrant:(NSUInteger)quadrant {
	CGRect frame = self.cursorView.frame;
	
	switch (quadrant) {
		case 1:
			frame.origin = CGPointMake(CGRectGetMaxX(self.frame) - frame.size.width, 0);
			break;
		case 2:
			frame.origin = CGPointZero;
			break;
		case 3:
			frame.origin = CGPointMake(0, CGRectGetMaxY(self.frame) - frame.size.height);
			break;
		case 4:
			frame.origin = CGPointMake(
									   CGRectGetMaxX(self.frame) - frame.size.width,
									   CGRectGetMaxY(self.frame) - frame.size.height);
			break;
	}
	
	self.cursorView.frame = frame;
}

- (void)calibrationDidEnd {
	NSLog(@"calibration ended");
	
    if (touchBasedDrawing) {
        [self.calibrationView removeFromSuperview];
        return;
    }
    
	__weak typeof(self) welf = self;
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
		__strong typeof(self) newSelf = welf;
		[newSelf.calibrationView removeFromSuperview];
	});
}

- (void)calibrationUserOptedForTouchDrawing {
    [self.x removeFromSuperview];
    [self.y removeFromSuperview];
    [self.mag removeFromSuperview];
    [self.angle removeFromSuperview];
    [self.xVector removeFromSuperview];
    
    touchBasedDrawing = YES;
}

#pragma mark - Original Code + idk

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [self.incrementalImage drawInRect:rect];
    [self.path stroke];
}

#pragma mark - Stylus Drawing Method

- (void)beginPointsWithPoint:(CGPoint)point {
    if (touchBasedDrawing) return;
	ctr = 0;
	pts[0] = point;
}

- (void)updatePointsWithPoint:(CGPoint)point {
    if (touchBasedDrawing) return;
	ctr ++;
	pts[ctr] = point;
	if (ctr == 4) {
		pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0); // move the endpoint to the middle of the line joining the second control point of the first Bezier segment and the first control point of the second Bezier segment
		
		[self.path moveToPoint:pts[0]];
		[self.path addCurveToPoint:pts[3] controlPoint1:pts[1] controlPoint2:pts[2]];
		
		[self setNeedsDisplay];
		// replace points and get ready to handle the next segment
		pts[0] = pts[3];
		pts[1] = pts[4];
		ctr = 1;
	}
}

- (void)endPointsWithPoint:(CGPoint)point {
    if (touchBasedDrawing) return;
    NSLog(@"endpoint method ran");
	if (!self.path.empty) {
		[self.paths addObject:[self.path copy]];
	}
	[self drawBitmap];
	[self setNeedsDisplay];
	[self.path removeAllPoints];
	ctr = 0;
}

#pragma mark - Touches Drawing Method

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!touchBasedDrawing) return;
    ctr = 0;
    UITouch *touch = [touches anyObject];
    pts[0] = [touch locationInView:self];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!touchBasedDrawing) return;
    UITouch *touch = [touches anyObject];
    CGPoint p = [touch locationInView:self];
    ctr++;
    pts[ctr] = p;
    if (ctr == 4) 
    {
        pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0); // move the endpoint to the middle of the line joining the second control point of the first Bezier segment and the first control point of the second Bezier segment 
        
        [self.path moveToPoint:pts[0]];
        [self.path addCurveToPoint:pts[3] controlPoint1:pts[1] controlPoint2:pts[2]];

        [self setNeedsDisplay];
        // replace points and get ready to handle the next segment
        pts[0] = pts[3]; 
        pts[1] = pts[4]; 
        ctr = 1;
    }
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!touchBasedDrawing) return;
	if (!self.path.empty) {
		[self.paths addObject:[self.path copy]];
	}
	[self drawBitmap];
	[self setNeedsDisplay];
	[self.path removeAllPoints];
	ctr = 0;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!touchBasedDrawing) return;
	[self touchesEnded:touches withEvent:event];
}

- (void)initalSetupWithPaths:(NSMutableArray *)initPaths {
    NSLog(@"READY");
    NSLog(@"%@", initPaths);
    [self.paths addObjectsFromArray:initPaths];
    [self.paths addObject:[UIBezierPath bezierPath]];
    [self undo];
}

- (void)saveWithTitle:(NSString *)title {
    [self setUserInteractionEnabled:NO];
    NSArray *filePaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([filePaths count] > 0) ? [filePaths objectAtIndex:0] : nil;
    NSString *dataPath = [basePath stringByAppendingPathComponent:@"Drawings"];
    NSString *location = [dataPath stringByAppendingPathComponent:title];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:location]) {
        NSLog(@"CREATING FOLDER!");
        NSError *error;
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
        NSLog(@"%@",error);
    }

    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:@{@"title": title, @"paths": self.paths} forKey:@"drawing"];
    NSLog(@"Title: %@", title);
    [archiver finishEncoding];
    [data writeToFile:location atomically:YES];
    NSLog(@"%@",location);
}

- (void)undo {
    self.path = nil;
    [self.paths removeLastObject];
    needsRedraw = true;
    [self drawBitmap];
    [self setNeedsDisplay];
}

- (void)drawBitmap
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, 0.0);
    if (needsRedraw) {
        self.incrementalImage = nil;
    }
    if (_incrementalImage == nil) // first time; paint background white
    {
        UIBezierPath *rectpath = [UIBezierPath bezierPathWithRect:self.bounds];
        [[UIColor whiteColor] setFill];
        [rectpath fill];
    }
    [self.incrementalImage drawAtPoint:CGPointZero];
    [[UIColor blackColor] setStroke];
    if (needsRedraw) {
        for (UIBezierPath *oldPath in self.paths) {
            [oldPath stroke];
        }
        needsRedraw = false;
    } else {
        [self.path stroke];
    }
    self.incrementalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

#pragma mark - Properties

- (UIBezierPath *)path {
    if (_path == nil) {
        _path = [UIBezierPath bezierPath];
        [_path setLineWidth:10.0];
    }
    return _path;
}

- (NSMutableArray *)paths {
    if (_paths == nil) {
        _paths = [[NSMutableArray alloc] init];
    }
    return _paths;
}

- (UIImage *)incrementalImage {
    if (_incrementalImage == nil) {
        _incrementalImage = [[UIImage alloc] init];
    }
    return _incrementalImage;
}

- (CalibrationView *)calibrationView {
	if (_calibrationView == nil) {
		_calibrationView = [[CalibrationView alloc] initWithFrame:CGRectZero];
	}
	return _calibrationView;
}

- (UILabel *)xVector {
    if (_xVector == nil) {
        _xVector = [[UILabel alloc] initWithFrame:CGRectZero];
    }
    return _xVector;
}

- (UIView *)cursorView {
	if (_cursorView == nil) {
		_cursorView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, 4)];
		_cursorView.backgroundColor = [UIColor redColor];
	}
	return _cursorView;
}

@end