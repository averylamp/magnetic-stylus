//
//  MagnetViewController.h
//  MagnetStylus
//
//  Created by Sebastian Cain on 1/16/15.
//  Copyright (c) 2015 PennAppsTeam2015. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MagnetViewController : UIViewController

@property (strong, nonatomic) UISlider *testSlider;

@end
